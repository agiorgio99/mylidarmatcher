#include "eigen_icp_2d.h"
#include "Eigen/Geometry"
#include "Eigen/Cholesky"
#include "rotations.h"
#include <iostream>
#include <ostream>
#include "ros/ros.h"
#include "std_msgs/String.h" //string messages
#include "geometry_msgs/Twist.h" //Vector3 messages (linear/angular)
#include "sensor_msgs/LaserScan.h" //laser scan message (header, angle_min, angle_max, 
//angle_increment, time_increment, scan_time, range_min, range_max, ranges, intensities)
#include "tf/tf.h"
#include "tf/message_filter.h"
#include "message_filters/subscriber.h"
#include "laser_geometry/laser_geometry.h"
#include <Eigen/StdVector>
#include <Eigen/Geometry>
#include <sstream> //Header providing string stream classes
#include <vector> //defines the vector container class
#include <fstream> //provides file stream classes
#include <memory> //manages dynamic memory
#include "tf2_msgs/TFMessage.h" //container allocator
#include <tf2_ros/buffer.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h> //convert ros messages to tf2 messages and to retrieve data from ros messages
#include <geometry_msgs/Pose2D.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>

//declaration of the ICP object
std::unique_ptr<ICP> ICP_object;
//declaration of the publisher
ros::Publisher publisher;

using Vector2f = Eigen::Vector2f;
using Isometry2f = Eigen::Isometry2f;
using ContainerType = std::vector<Vector2f, Eigen::aligned_allocator<Vector2f> >;

//boolean variable, flase only in the first iteration of the ICPSCanCallBack, then it will always remain true
bool start_of_matching = false;

Isometry2f Map_Base, Base_Laser, Map_Laser, Odom_Base, Base_Odom, Map_Odom;

void ICPScanCallback(const sensor_msgs::LaserScan::ConstPtr& scan_msg){

	//getting parameters useful for computing the size of the scan
	float angle_min = scan_msg->angle_min;
	float angle_max = scan_msg->angle_max;
	float angle_increment = scan_msg->angle_increment;
	//size of the point clouds received from the laser scanner
	float size = std::ceil((angle_max-angle_min)/angle_increment);

	float angle = angle_min;
	//initialization of the new point_cloud obtained by the laser scanner
	std::vector<Vector2f> new_cloud(size);
	int i;
	for (i=0; i<size; i++) {
		float point_value = scan_msg->ranges[i];
		angle += angle_increment;
		float point_x = point_value*cos(angle);
		float point_y = point_value*sin(angle);
		new_cloud[i]= Vector2f(point_x, point_y);
	}
	
	if(!start_of_matching){
		//if we are at the first iteration, we have only one point_cloud and we haven't set all the parameters needed yet,
		//so we build the ICP_object and stop the callback
		ContainerType fixed(size);
		ContainerType moving(size);
		ICP_object = std::unique_ptr<ICP>(new ICP(fixed, moving, 20));
		ICP_object->set_fixed(new_cloud);
		Map_Base = ICP_object->IsometryGenerator("map", "base_link");
		Base_Laser = ICP_object->IsometryGenerator("base_link", "base_laser_link");
		ICP_object->set_Map_Base(Map_Base);
		ICP_object->set_Base_Laser(Base_Laser);
		ICP_object->set_Map_Laser();
		Map_Laser= ICP_object->Map_Laser();
		start_of_matching=true;
		return;
	}
	//if we are here, it means we are after the first iteration of the callback,
	//so we have more than one scan and we can start ICP
	ICP_object->set_moving(new_cloud);

	std::cerr << "The ICP algorithm is starting..." << std::endl;
	ICP_object->run(10);
	std::cerr << "...the ICP algorithm is ended!" << std::endl;

	//after ICP algorithm is executed, we don't need fixed/moving in this configuration anymore,
	//so we swap them in preparation to the next callback
	ICP_object->switch_clouds();

	//now that we have a new isometry of correspondences X, we update the isometry that allows us to go from the map-frame to the base_laser_link-frame
	ICP_object->update_Map_Laser();
	Map_Laser= ICP_object->Map_Laser();
	//we also update the isometry that allows us to go from the map-frame to the base_link-frame,
	//taking the product between the updated version of Map_Laser and the inverse of matrix Base_Laser
	ICP_object->update_Map_Base(); 
	Map_Base = ICP_object->Map_Base();

	//then we obtain the new pose
	geometry_msgs::Pose2D::Ptr pose;
	//Now we use boost::make_shared() to create a smart pointer of type boost::shared_ptr 
	//without having to calling the constructor of boost::shared_ptr ourselves.
	pose = boost::make_shared<geometry_msgs::Pose2D>();
	
	pose->x = Map_Base.translation()(0);
	pose->y = Map_Base.translation()(1);
	pose->theta = Eigen::Rotation2Df(Map_Base.rotation()).angle();

	//finally we publish the pose into the /pose2D topic
	publisher.publish(pose); 
	
	//we use odom system in order to broadcast transformations that represent the changing position of the robot
	Odom_Base = ICP_object->IsometryGenerator("odom","base_link");
	ICP_object->set_Odom_Base(Odom_Base);
	//taking the the isometry that allows us to go from the map-frame to the odom-frame,
	//with the product between the update version of Map_Base and the inverse of matrix Odom_Base
	Map_Odom = ICP_object->Map_Odom();

	//std::cerr << "Matrix Map_Odom: " << std::endl;
	//std::cerr << Map_Odom.matrix() << std::endl;

	//here there's the procedure to broadcast the transformation Map_Odom (see roswiki)
	static tf2_ros::TransformBroadcaster br;
	geometry_msgs::TransformStamped ts;
	ts.header.stamp = ros::Time::now();
	ts.header.frame_id = "/map";
	ts.child_frame_id = "/odom";
	ts.transform.translation.x = Map_Odom.translation()(0);
	ts.transform.translation.y = Map_Odom.translation()(1);
	ts.transform.translation.z = 0.0;
	tf2::Quaternion q;
	q.setRPY(0, 0, Eigen::Rotation2Df(Map_Odom.rotation()).angle());
	ts.transform.rotation.x = q.x();
	ts.transform.rotation.y = q.y();
	ts.transform.rotation.z = q.z();
	ts.transform.rotation.w = q.w(); 
	br.sendTransform(ts); 
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "mylidarmatcher");
	//declaration of the rosnode
	ros::NodeHandle n;  
	//subscription to the topic of the laserScan, providing points passed to the callback responsible for the ICP procedure
  	ros::Subscriber laserScanSub = n.subscribe("/base_scan", 1000, ICPScanCallback);
	//initialization of the publisher, where we publish the pose message
	publisher = n.advertise<geometry_msgs::Pose2D>("/pose2D", 1000);

	ros::spin(); 
	
	return 0;
}