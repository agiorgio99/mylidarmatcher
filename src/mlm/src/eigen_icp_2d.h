#pragma once
#include "eigen_kdtree.h"
#include "Eigen/Geometry"
#include <iostream>
#include <ostream>
#include <list>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/buffer.h>
#include "rotations.h"
#include "tf/tf.h"

using Vector2f = Eigen::Vector2f;
using Isometry2f = Eigen::Isometry2f;

class ICP {
protected:
  struct PointPair{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    PointPair(const Vector2f& fixed_, const Vector2f& moving_):
      _fixed(fixed_),
      _moving(moving_){};
    
    PointPair(){}
    Vector2f _fixed;
    Vector2f _moving;
  };
  using PointPairVector=std::vector<PointPair, Eigen::aligned_allocator<PointPair>>;


public:
  using ContainerType = std::vector<Vector2f, Eigen::aligned_allocator<Vector2f> >;

  ICP(ContainerType& fixed_,
      ContainerType& moving_,
      int min_points_in_leaf);

  void computeCorrespondences();
  
  // for me to test
  void computeCorrespondencesFake();
  
  void optimizeCorrespondences();
  
  void run(int max_iterations);

  void draw(std::ostream& os);
  
  const Eigen::Isometry2f& X() const {return _X;}
  Eigen::Isometry2f& X()  {return _X;}
  inline int numCorrespondences() const {return _correspondences.size();}
  inline int numKernelized() const {return _num_kernelized;}
  inline int numInliers() const {return _num_inliers;}
  inline const Eigen::Matrix<float, 3,1>& dx() const {return _dx;}

  Isometry2f Map_Base() {return _Map_Base;}
  Isometry2f Base_Laser() {return _Base_Laser;}
  Isometry2f Map_Laser() {return _Map_Laser;}
  Isometry2f Odom_Base() {return _Odom_Base;}
  Isometry2f Base_Odom() {return _Odom_Base.inverse();}
  Isometry2f Map_Odom() {return _Map_Base*(_Odom_Base.inverse());}

  void set_Map_Base(Isometry2f Map_Base_) {_Map_Base=Map_Base_;}
  void set_Base_Laser(Isometry2f Base_Laser_) {_Base_Laser=Base_Laser_;}
  void set_Odom_Base(Isometry2f Odom_Base_) {_Odom_Base=Odom_Base_;}
  void set_Map_Laser() {_Map_Laser=_Map_Base*_Base_Laser;}

  void set_fixed(std::vector<Vector2f> fixed_) {
    int i;
    int fixed_size= fixed_.size();
    _fixed.resize(fixed_size);
    for(i=0; i<fixed_size; i++){
      _fixed[i]=fixed_[i];
    }
  }

  void set_moving(std::vector<Vector2f> moving_) {
    int i;
    int moving_size= moving_.size();
    _moving.resize(moving_size);
    for(i=0; i<moving_size; i++){
      _moving[i]=moving_[i];
    }
  }

  void print_fixed(){
    std::cerr << "La size di fixed è " << _fixed.size() << std::endl;
    int i;
		for(i=0; i<_fixed.size(); i++){
			std::cerr << "Vector2f n°: " << i;
			std::cerr << " x= " << _fixed[i][0];
			std::cerr << " y= " << _fixed[i][1] << std::endl;
		}
  }

  void print_moving(){
    std::cerr << "La size di moving è " << _moving.size() << std::endl;
    int k;
    for(k=0; k<_fixed.size(); k++){
      std::cerr << "Vector2f n°: " << k;
      std::cerr << " x= " << _moving[k][0];
      std::cerr << " y= " << _moving[k][1] << std::endl;
    }
  }

  void switch_clouds(){
    _moving.swap(_fixed);
  }

  void update_Map_Laser(){
    _Map_Laser=_Map_Laser*_X;
  }

  void update_Map_Base(){
    _Map_Base=_Map_Laser*(_Base_Laser.inverse());
  }

  Isometry2f IsometryGenerator(const std::string& src_frame, const std::string& dest_frame) {
    
    Eigen::Isometry2f T = Eigen::Isometry2f::Identity();
    geometry_msgs::TransformStamped transformStamped;
    tf2::Quaternion q;
    double roll, pitch, yaw;
    //declaration of the listener that will be useful for trasformations
    tf2_ros::Buffer listener;
    //creation of the TransformListener object 
	  tf2_ros::TransformListener tfListener(listener);

    try {
      transformStamped = listener.lookupTransform(dest_frame, src_frame, ros::Time(0), ros::Duration(4.0));
      tf2::convert(transformStamped.transform.rotation , q);
      tf2::Matrix3x3 m(q);
      m.getRPY(roll, pitch, yaw);
      auto tr = transformStamped.transform.translation;
      T.linear()=Rtheta(yaw); 
      T.translation()=Vector2f(tr.x, tr.y);
    }
    catch ( tf::TransformException& e ) {
      std::cout << e.what();
      std::cerr << "cannot get transformation correctly" << std::endl;
    }

    return T;
  }
  
protected:
  using TreeNodeType = TreeNode_<typename ContainerType::iterator>;
  
  ContainerType _fixed;
  ContainerType _moving;

  Isometry2f _Map_Base;
  Isometry2f _Base_Laser;
  Isometry2f _Map_Laser;
  Isometry2f _Odom_Base;

  Eigen::Isometry2f _X=Eigen::Isometry2f::Identity();
  std::unique_ptr<TreeNodeType> _kd_tree;
  int _min_points_in_leaf;
  float _ball_radius=10.f;
  float _kernel_chi2 = 1.f;
  float _chi2_sum=0;

  PointPairVector _correspondences;
  int _num_kernelized=0;
  int _num_inliers=0;
  Eigen::Matrix<float, 3,1> _dx;
};
