# mylidarmatcher
Robot Programming Project 2021-22

# title
2D Lidar Matcher 

# description 
Implement a 2D scan matcher, working with 2d laser scans (integrated in ROS)

# installation

- download course folder by cloning repo on gitlab (execute on terminal: 'git clone "https://gitlab.com/grisetti/robotprogramming_2021_22"')

- to configure the environment, follow instructions in README.md file at "robotprogramming_2021_22/source/srrg2_workspace" (ROS and catkin_workspace needed)

- download project folder by cloning repo on gitlab (execute on terminal: 'git clone "https://gitlab.com/agiorgio99/mylidarmatcher"')

- to move the robot during testing, install useful keboard (execute on terminal: 'sudo apt-get install ros-<ros_version>-teleop-twist-keyboard

# testing

Open three terminals (TX= terminal X):

T1) ROS SLAM/RViz environment:

- cd ~/<path>/robotprogramming_2021_22/source/srrg2_workspace/src/srrg2_navigation_2d/config

- source ~/<path>/robotprogramming_2021_22/source/srrg2_workspace/devel/setup.bash

- ~/<path>/robotprogramming_2021_22/source/srrg2_workspace/src/srrg2_webctl/proc_webctl run_navigation.webctl

Open browser, connect at port 9001 by going on "localhost:9001" and click on the first 4 start buttons (Roscore, StageRos, MapServer and RViz). Keep only RViz window displayed: here it is needed to change the fixed frame (upper-left corner) in "odom", then click on "add" (lower-left corner) and select "LaserScan", finally expand the LaserScan options (on the left) and change topic in "/base_scan" and size from 0.01 to 0.1 . 

T2) Executable/Main of the project:

- cd <path>/mylidarmatcher/

- catkin_make

- source devel/setup.bash

- rosrun mylidarmatcher mylidarmatcher

At this point, the ICP algorithm starts and it is needed to change on RViz window the fixed frame from "odom" to "map" (once it appears between the selectable frames), in order to see the points scanned by the laser scan.

T3) Keyboard/move commands:

- rosrun teleop_twist_keyboard teleop_twist_keyboard.py

At this point, see instructions displayed to move the robot and try the magic!



